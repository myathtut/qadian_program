package com.example.myathtut.qadianprogram.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.adapters.AdapterDay1;
import com.example.myathtut.qadianprogram.adapters.Days3Adapter;
import com.example.myathtut.qadianprogram.data.vos.model.Da1model;
import com.example.myathtut.qadianprogram.data.vos.model.Days3Model;
import com.example.myathtut.qadianprogram.view.Day1ViewHolder;
import com.example.myathtut.qadianprogram.view.Days3ViewHolder;

/**
 * Created by myathtut on 12/10/16.
 */
public class Days3Fragment extends Fragment {
    private Days3Adapter days3Adapter;
    private Days3ViewHolder.ControllerDays3 controllerDays3;


    public static Days3Fragment newInstance() {
        return new Days3Fragment();
    }

    public Days3Fragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        controllerDays3 = (Days3ViewHolder.ControllerDays3) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        days3Adapter = new Days3Adapter(Days3Model.getInstance().getday1list(), controllerDays3);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day3, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_tests);
        recyclerView.setAdapter(days3Adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return view;
    }


}
