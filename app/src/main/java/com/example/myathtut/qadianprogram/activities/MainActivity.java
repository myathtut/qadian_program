package com.example.myathtut.qadianprogram.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.data.vos.Day2VO;
import com.example.myathtut.qadianprogram.data.vos.Days3VO;
import com.example.myathtut.qadianprogram.data.vos.NoteVOS;
import com.example.myathtut.qadianprogram.fragment.TabActivityFragment;
import com.example.myathtut.qadianprogram.view.Day1ViewHolder;
import com.example.myathtut.qadianprogram.view.Day2ViewHolder;
import com.example.myathtut.qadianprogram.view.Days3ViewHolder;
import com.example.myathtut.qadianprogram.view.NoViewHolder;

public class MainActivity extends AppCompatActivity implements
        Day1ViewHolder.ControllerDay1, Day2ViewHolder.ControllerDay2, Days3ViewHolder.ControllerDays3, NoViewHolder.ControllerNote {


    private void ex() {
        Intent intent = NoteActivity.newIntent();
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (savedInstanceState == null) {
            TabActivityFragment fragment = TabActivityFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTap(Day1VO day1VO) {

    }

    @Override
    public void onTap(Day2VO day2VO) {

    }

//    @Override
//    public void onTap(Day3VO day3VO) {
//
//    }

    @Override
    public void onTap(Days3VO days3VO) {

    }

    @Override
    public void onTap(NoteVOS noteVOS) {

    }
}
