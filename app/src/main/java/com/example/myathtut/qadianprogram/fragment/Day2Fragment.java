package com.example.myathtut.qadianprogram.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.R;

import com.example.myathtut.qadianprogram.adapters.Day2Adapter;
import com.example.myathtut.qadianprogram.data.vos.model.Day2Model;
import com.example.myathtut.qadianprogram.view.Day2ViewHolder;

/**
 * Created by myathtut on 12/2/16.
 */
public class Day2Fragment extends Fragment {
    private Day2Adapter day2Adapter;
    private Day2ViewHolder.ControllerDay2 mcontrollerDay2;

    public static Day2Fragment newInstance() {
        return new Day2Fragment();
    }

    public Day2Fragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mcontrollerDay2 = (Day2ViewHolder.ControllerDay2) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        day2Adapter = new Day2Adapter(Day2Model.getInstance().getday1list(), mcontrollerDay2);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day2, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_tests);
        recyclerView.setAdapter(day2Adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return view;
    }


}
