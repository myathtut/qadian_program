package com.example.myathtut.qadianprogram.data.vos.model;

import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.util.CommonInstances;
import com.example.myathtut.qadianprogram.util.Jsonutil;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by myathtut on 11/28/16.
 */
public class Da1model {
    private static final String DUMMY_EVENT_LIST = "event_list.json";
    private static Da1model objInstance;
    private List<Day1VO> day1VOs;

    private Da1model()
    {
        day1VOs=initializeEventList();

    }

    public static Da1model getInstance(){
        if(objInstance == null) {
            objInstance = new Da1model();
        }

        return objInstance;
    }

    private List<Day1VO> initializeEventList() {
        List<Day1VO> eventList = new ArrayList<>();

        try {
            String dummyEventList = Jsonutil.getInstance().loadDummyData(DUMMY_EVENT_LIST);
            Type listType = new TypeToken<List<Day1VO>>() {
            }.getType();
            eventList = CommonInstances.getGsonInstance().fromJson(dummyEventList, listType);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public List<Day1VO> getday1list()
    {
        return day1VOs;
    }

    public Day1VO getEventByTitle(String  title) {
        for (Day1VO dec : day1VOs) {
            if (dec.getEventTitle().equals(title)) {
                return dec;
            }
        }
        return null;
    }
}
