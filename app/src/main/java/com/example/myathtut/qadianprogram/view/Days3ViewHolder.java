package com.example.myathtut.qadianprogram.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Days3VO;

/**
 * Created by myathtut on 12/10/16.
 */
public class Days3ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView tvEventDesc, tvEventTitle;
    private TextView tvEventTime;
    private Days3VO days3VO;
    private ControllerDays3 mcontrollerDay2;

    public Days3ViewHolder(View view, ControllerDays3 controllerDays3) {
        super(view);
        mcontrollerDay2=controllerDays3;

        tvEventTitle = (TextView) view.findViewById(R.id.tv_event_title);
        tvEventDesc = (TextView) view.findViewById(R.id.tv_event_desc);
        tvEventTime = (TextView) view.findViewById(R.id.tv_event_time);
    }

    public void setData(Days3VO days3VO) {
        this.days3VO =days3VO;

        tvEventTitle.setText(days3VO.getEventTitle());
        tvEventDesc.setText(days3VO.getEventDesc());
        tvEventTime.setText(days3VO.getEventTime()); //Friday, Feb 26, 1:00pm - 5:00pm

    }
    @Override
    public void onClick(View view) {
        mcontrollerDay2.onTap(days3VO);
    }

    public interface ControllerDays3 {
        void onTap(Days3VO days3VO);
    }

}
