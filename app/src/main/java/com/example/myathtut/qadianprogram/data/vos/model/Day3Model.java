package com.example.myathtut.qadianprogram.data.vos.model;

import com.example.myathtut.qadianprogram.data.vos.Day2VO;
import com.example.myathtut.qadianprogram.data.vos.Day3VO;
import com.example.myathtut.qadianprogram.util.CommonInstances;
import com.example.myathtut.qadianprogram.util.Jsonutil;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by myathtut on 12/3/16.
 */
public class Day3Model {

    private static final String DUMMY_EVENT_LIST = "event_list2.json";
    private static Day3Model objInstance;
    private List<Day3VO> day3VOs;

    private Day3Model()
    {
        day3VOs=initializeEventList();

    }

    public static Day3Model getInstance(){
        if(objInstance == null) {
            objInstance = new Day3Model();
        }

        return objInstance;
    }

    private List<Day3VO> initializeEventList() {
        List<Day3VO> eventList = new ArrayList<>();

        try {
            String dummyEventList = Jsonutil.getInstance().loadDummyData(DUMMY_EVENT_LIST);
            Type listType = new TypeToken<List<Day2VO>>() {
            }.getType();
            eventList = CommonInstances.getGsonInstance().fromJson(dummyEventList, listType);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public List<Day3VO> getday1list()
    {
        return day3VOs;
    }

    public Day3VO getEventByTitle(String  title) {
        for (Day3VO dec : day3VOs) {
            if (dec.getEventTitle().equals(title)) {
                return dec;
            }
        }
        return null;
    }
}
