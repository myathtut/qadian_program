package com.example.myathtut.qadianprogram.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.data.vos.Day2VO;

/**
 * Created by myathtut on 12/2/16.
 */
public class Day2ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView tvEventDesc, tvEventTitle;
    private TextView tvEventTime;
    private Day2VO mday2VO;
    private ControllerDay2 mcontrollerDay2;

    public Day2ViewHolder(View view,ControllerDay2 controllerDay2) {
        super(view);
        mcontrollerDay2=controllerDay2;

        tvEventTitle = (TextView) view.findViewById(R.id.tv_event_title);
        tvEventDesc = (TextView) view.findViewById(R.id.tv_event_desc);
        tvEventTime = (TextView) view.findViewById(R.id.tv_event_time);
    }

    public void setData(Day2VO day2VO) {
        this.mday2VO=day2VO;

        tvEventTitle.setText(day2VO.getEventTitle());
        tvEventDesc.setText(day2VO.getEventDesc());
        tvEventTime.setText(day2VO.getEventTime()); //Friday, Feb 26, 1:00pm - 5:00pm

    }
    @Override
    public void onClick(View view) {
        mcontrollerDay2.onTap(mday2VO);
    }

    public interface ControllerDay2 {
        void onTap(Day2VO day2VO);
    }
}
