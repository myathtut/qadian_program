package com.example.myathtut.qadianprogram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.Qadian;
import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.view.Day1ViewHolder;

import java.util.List;

/**
 * Created by myathtut on 11/28/16.
 */
public class AdapterDay1 extends RecyclerView.Adapter<Day1ViewHolder> {
    private LayoutInflater inflater;
    private List<Day1VO> day1VOList;
    private Day1ViewHolder.ControllerDay1 mcontrollerDay1;


    public AdapterDay1(List<Day1VO> day1VOList,Day1ViewHolder.ControllerDay1 mcontrollerDay1) {
        inflater = LayoutInflater.from(Qadian.getContext());
        this.day1VOList = day1VOList;
        this.mcontrollerDay1=mcontrollerDay1;
    }

    @Override
    public Day1ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_item_day1, parent, false);
        final Day1ViewHolder day1ViewHolder = new Day1ViewHolder(view,mcontrollerDay1);
        return day1ViewHolder;
    }

    @Override
    public void onBindViewHolder(Day1ViewHolder holder, int position) {
        holder.setData(day1VOList.get(position));
    }

    @Override
    public int getItemCount() {
        return day1VOList.size();
    }
}
