package com.example.myathtut.qadianprogram.util;

import com.google.gson.Gson;

/**
 * Created by myathtut on 11/28/16.
 */
public class CommonInstances {

    private static Gson gson = new Gson();

    public static Gson getGsonInstance()
    {
        return gson;
    }
}
