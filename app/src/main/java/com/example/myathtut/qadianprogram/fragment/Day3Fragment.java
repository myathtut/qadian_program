package com.example.myathtut.qadianprogram.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.adapters.Day3Adapter;
import com.example.myathtut.qadianprogram.data.vos.model.Day3Model;
import com.example.myathtut.qadianprogram.view.Day3ViewHolder;

/**
 * Created by myathtut on 12/3/16.
 */
public class Day3Fragment extends Fragment {

    private Day3Adapter day3Adapter;
    private Day3ViewHolder.ControllerDay3 mcontrollerDay3;

    public static Day3Fragment newInstance() {
        return new Day3Fragment();
    }

    public Day3Fragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mcontrollerDay3=(Day3ViewHolder.ControllerDay3) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        day3Adapter= new Day3Adapter(Day3Model.getInstance().getday1list(),mcontrollerDay3);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day3, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_tests);
        recyclerView.setAdapter(day3Adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return view;
    }
}
