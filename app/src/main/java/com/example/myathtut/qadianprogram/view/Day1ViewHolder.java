package com.example.myathtut.qadianprogram.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day1VO;

/**
 * Created by myathtut on 11/28/16.
 */
public class Day1ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tvEventDesc, tvEventTitle;
    private TextView tvEventTime;
    private Day1VO mday1VO;
    private ControllerDay1 mcontrollerDay1;

    public Day1ViewHolder(View view, ControllerDay1 controllerDay1) {
        super(view);
        mcontrollerDay1 = controllerDay1;

        tvEventTitle = (TextView) view.findViewById(R.id.tv_event_title);
        tvEventDesc = (TextView) view.findViewById(R.id.tv_event_desc);
        tvEventTime = (TextView) view.findViewById(R.id.tv_event_time);
    }

    public void setData(Day1VO day1VO) {
        this.mday1VO = day1VO;

        tvEventTitle.setText(day1VO.getEventTitle());
        tvEventDesc.setText(day1VO.getEventDesc());
        tvEventTime.setText(day1VO.getEventTime()); //Friday, Feb 26, 1:00pm - 5:00pm

    }

    @Override
    public void onClick(View view) {
        mcontrollerDay1.onTap(mday1VO);
    }

    public interface ControllerDay1 {
        void onTap(Day1VO day1VO);
    }
}
