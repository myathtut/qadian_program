package com.example.myathtut.qadianprogram.data.vos.model;

import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.data.vos.Days3VO;
import com.example.myathtut.qadianprogram.util.CommonInstances;
import com.example.myathtut.qadianprogram.util.Jsonutil;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by myathtut on 12/10/16.
 */
public class Days3Model {
    private static final String DUMMY_EVENT_LIST = "event_list2.json";
    private static Days3Model objInstance;
    private List<Days3VO> days3VOs;

    private Days3Model()
    {
        days3VOs =initializeEventList();

    }

    public static Days3Model getInstance(){
        if(objInstance == null) {
            objInstance = new Days3Model();
        }

        return objInstance;
    }

    private List<Days3VO> initializeEventList() {
        List<Days3VO> eventList = new ArrayList<>();

        try {
            String dummyEventList = Jsonutil.getInstance().loadDummyData(DUMMY_EVENT_LIST);
            Type listType = new TypeToken<List<Days3VO>>() {
            }.getType();
            eventList = CommonInstances.getGsonInstance().fromJson(dummyEventList, listType);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public List<Days3VO> getday1list()
    {
        return days3VOs;
    }

    public Days3VO getEventByTitle(String  title) {
        for (Days3VO dec : days3VOs) {
            if (dec.getEventTitle().equals(title)) {
                return dec;
            }
        }
        return null;
    }


}
