package com.example.myathtut.qadianprogram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.Qadian;
import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Days3VO;
import com.example.myathtut.qadianprogram.view.Days3ViewHolder;

import java.util.List;

/**
 * Created by myathtut on 12/10/16.
 */
public class Days3Adapter extends RecyclerView.Adapter<Days3ViewHolder> {
    private LayoutInflater inflater;
    private List<Days3VO> days3VOList;
    private Days3ViewHolder.ControllerDays3 controllerDays3;


    public Days3Adapter(List<Days3VO> days3VOList, Days3ViewHolder.ControllerDays3 controllerDays3) {
        inflater = LayoutInflater.from(Qadian.getContext());
        this.days3VOList = days3VOList;
        this.controllerDays3 = controllerDays3;
    }

    @Override
    public Days3ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_item_day3, parent, false);
        final Days3ViewHolder days3ViewHolder = new Days3ViewHolder(view, controllerDays3);
        return days3ViewHolder;
    }

    @Override
    public void onBindViewHolder(Days3ViewHolder holder, int position) {
        holder.setData(days3VOList.get(position));

    }

    @Override
    public int getItemCount() {
        return days3VOList.size();
    }
}
