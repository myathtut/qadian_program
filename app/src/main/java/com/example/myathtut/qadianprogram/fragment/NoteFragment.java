package com.example.myathtut.qadianprogram.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.adapters.Day2Adapter;
import com.example.myathtut.qadianprogram.adapters.NoteAdapter;
import com.example.myathtut.qadianprogram.data.vos.model.Day2Model;
import com.example.myathtut.qadianprogram.data.vos.model.Notemodel;
import com.example.myathtut.qadianprogram.view.Day2ViewHolder;
import com.example.myathtut.qadianprogram.view.NoViewHolder;

/**
 * Created by myathtut on 12/12/16.
 */
public class NoteFragment extends Fragment {
    private NoteAdapter noteAdapter;
    private NoViewHolder.ControllerNote controllerNote;


    public static NoteFragment newInstance() {
        return new NoteFragment();
    }

    public NoteFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        controllerNote = (NoViewHolder.ControllerNote) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        noteAdapter = new NoteAdapter(Notemodel.getInstance().getday1list(), controllerNote);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_tests);
        recyclerView.setAdapter(noteAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return view;
    }

}
