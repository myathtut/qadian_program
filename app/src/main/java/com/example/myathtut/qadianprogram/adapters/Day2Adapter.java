package com.example.myathtut.qadianprogram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.Qadian;
import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.data.vos.Day2VO;
import com.example.myathtut.qadianprogram.view.Day1ViewHolder;
import com.example.myathtut.qadianprogram.view.Day2ViewHolder;

import java.util.List;

/**
 * Created by myathtut on 12/2/16.
 */
public class Day2Adapter extends RecyclerView.Adapter<Day2ViewHolder> {
    private LayoutInflater inflate;
    private List<Day2VO> day2VOs;
    private Day2ViewHolder.ControllerDay2 mcontrollerDay2;

    public Day2Adapter(List<Day2VO> day2VO,Day2ViewHolder.ControllerDay2 mcontrollerDay2) {
        inflate = LayoutInflater.from(Qadian.getContext());
        this.day2VOs = day2VO;
        this.mcontrollerDay2=mcontrollerDay2;


    }

    @Override
    public Day2ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflate.inflate(R.layout.view_item_day2, parent, false);
        final Day2ViewHolder day2ViewHolder = new Day2ViewHolder(view,mcontrollerDay2);
        return day2ViewHolder;
    }

    @Override
    public void onBindViewHolder(Day2ViewHolder holder, int position) {
        holder.setData(day2VOs.get(position));
    }

    @Override
    public int getItemCount() {
        return day2VOs.size();
    }
}
