package com.example.myathtut.qadianprogram.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day3VO;

/**
 * Created by myathtut on 12/3/16.
 */
public class Day3ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tvEventDesc, tvEventTitle;
    private TextView tvEventTime;
    private Day3VO mday3VO;
    private ControllerDay3 mcontrollerDay3;

    public Day3ViewHolder(View view, ControllerDay3 controllerDay3) {
        super(view);
        mcontrollerDay3 = controllerDay3;
        tvEventTitle = (TextView) view.findViewById(R.id.tv_event_title);
        tvEventDesc = (TextView) view.findViewById(R.id.tv_event_desc);
        tvEventTime = (TextView) view.findViewById(R.id.tv_event_time);
    }

    public void setData(Day3VO day3VO) {
        this.mday3VO = day3VO;

        tvEventTitle.setText(day3VO.getEventTitle());
        tvEventDesc.setText(day3VO.getEventDesc());
        tvEventTime.setText(day3VO.getEventTime()); //Friday, Feb 26, 1:00pm - 5:00pm

    }

    @Override
    public void onClick(View view) {
        mcontrollerDay3.onTap(mday3VO);
    }

    public interface ControllerDay3 {

        void onTap(Day3VO day3VO);

    }
}
