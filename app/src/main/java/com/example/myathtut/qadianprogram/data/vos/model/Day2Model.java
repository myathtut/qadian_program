package com.example.myathtut.qadianprogram.data.vos.model;

import com.example.myathtut.qadianprogram.data.vos.Day1VO;
import com.example.myathtut.qadianprogram.data.vos.Day2VO;
import com.example.myathtut.qadianprogram.util.CommonInstances;
import com.example.myathtut.qadianprogram.util.Jsonutil;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by myathtut on 12/2/16.
 */
public class Day2Model {

    private static final String DUMMY_EVENT_LIST = "event_list1.json";
    private static Day2Model objInstance;
    private List<Day2VO> day2VOs;

    private Day2Model()
    {
        day2VOs=initializeEventList();

    }

    public static Day2Model getInstance(){
        if(objInstance == null) {
            objInstance = new Day2Model();
        }

        return objInstance;
    }

    private List<Day2VO> initializeEventList() {
        List<Day2VO> eventList = new ArrayList<>();

        try {
            String dummyEventList = Jsonutil.getInstance().loadDummyData(DUMMY_EVENT_LIST);
            Type listType = new TypeToken<List<Day2VO>>() {
            }.getType();
            eventList = CommonInstances.getGsonInstance().fromJson(dummyEventList, listType);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public List<Day2VO> getday1list()
    {
        return day2VOs;
    }

    public Day2VO getEventByTitle(String  title) {
        for (Day2VO dec : day2VOs) {
            if (dec.getEventTitle().equals(title)) {
                return dec;
            }
        }
        return null;
    }
}
