package com.example.myathtut.qadianprogram.activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class Tab2ActivityFragment extends Fragment {

    public Tab2ActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab2, container, false);
    }
}
