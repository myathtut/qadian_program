package com.example.myathtut.qadianprogram.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.myathtut.qadianprogram.Qadian;
import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day2VO;
import com.example.myathtut.qadianprogram.data.vos.Days3VO;
import com.example.myathtut.qadianprogram.view.Day2ViewHolder;
import com.example.myathtut.qadianprogram.view.Days3ViewHolder;

public class NoteActivity extends AppCompatActivity implements Day2ViewHolder.ControllerDay2,Days3ViewHolder.ControllerDays3 {
    public static Intent newIntent() {
        Intent intent = new Intent(Qadian.getContext(), NoteActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        if (savedInstanceState == null) {
//            NoteActivityFragment fragment = NoteActivityFragment.newInstance();
//            getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.fl_container, fragment)
//                    .commit();
//        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

//    @Override
//    public void onTap(Day1VO day1VO) {
//
//    }

    @Override
    public void onTap(Day2VO day2VO) {

    }

    @Override
    public void onTap(Days3VO days3VO) {

    }
}
