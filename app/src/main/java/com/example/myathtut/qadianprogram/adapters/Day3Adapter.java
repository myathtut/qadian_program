package com.example.myathtut.qadianprogram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.Qadian;
import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.Day3VO;
import com.example.myathtut.qadianprogram.view.Day3ViewHolder;

import java.util.List;

/**
 * Created by myathtut on 12/3/16.
 */
public class Day3Adapter extends RecyclerView.Adapter<Day3ViewHolder> {
    private LayoutInflater inflater;
    private List<Day3VO> day3VOs;
    private Day3ViewHolder.ControllerDay3 mcontrollerDay3;

    public Day3Adapter(List<Day3VO> day3VOs, Day3ViewHolder.ControllerDay3 mcontrollerDay3) {
        inflater = LayoutInflater.from(Qadian.getContext());
        this.day3VOs = day3VOs;
        this.mcontrollerDay3 = mcontrollerDay3;
    }

    @Override
    public Day3ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_item_day3, parent, false);
        final Day3ViewHolder day3ViewHolder = new Day3ViewHolder(view, mcontrollerDay3);
        return day3ViewHolder;
    }

    @Override
    public void onBindViewHolder(Day3ViewHolder holder, int position) {
        holder.setData(day3VOs.get(position));

    }

    @Override
    public int getItemCount() {
        return day3VOs.size();
    }
}
