package com.example.myathtut.qadianprogram.util;

import android.content.Context;

import com.example.myathtut.qadianprogram.Qadian;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by myathtut on 11/28/16.
 */
public class Jsonutil {

    private static final String PATH_DUMMY_DATA = "dummy_data";

    private static Jsonutil objInstace;

    private Context context;

    public static Jsonutil getInstance() {
        if (objInstace == null) {
            objInstace = new Jsonutil();
        }

        return objInstace;
    }

    private Jsonutil()
    {
        context= Qadian.getContext();
    }

    private byte[] readJsonFile(String fileName) throws IOException {
        InputStream inStream = context.getAssets().open(fileName);
        int size = inStream.available();
        byte[] buffer = new byte[size];
        inStream.read(buffer);
        inStream.close();
        return buffer;
    }

    public String loadDummyData(String fileName) throws IOException, JSONException {
        byte[] buffer = readJsonFile(PATH_DUMMY_DATA + "/" + fileName);
        return new String(buffer, "UTF-8").toString();
    }
}
