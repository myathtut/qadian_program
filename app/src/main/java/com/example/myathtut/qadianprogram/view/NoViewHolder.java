package com.example.myathtut.qadianprogram.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.NoteVOS;

/**
 * Created by myathtut on 12/14/16.
 */
public class NoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tvEventDesc, tvEventTitle;
    private TextView tvEventTime;
    private NoteVOS noteVOS;
    private ControllerNote controllerNote;

    public NoViewHolder(View view, ControllerNote controllerNote1) {
        super(view);

        controllerNote = controllerNote1;
        tvEventTitle = (TextView) view.findViewById(R.id.tv_event_title);
        tvEventDesc = (TextView) view.findViewById(R.id.tv_event_desc);
        tvEventTime = (TextView) view.findViewById(R.id.tv_event_time);
    }

    public void setData(NoteVOS noteVOS) {
        this.noteVOS = noteVOS;

        tvEventTitle.setText(noteVOS.getEventTitle());
        tvEventDesc.setText(noteVOS.getEventDesc());
//        tvEventTime.setText(noteVOS.getEventTime()); //Friday, Feb 26, 1:00pm - 5:00pm

    }

    @Override
    public void onClick(View view) {

        controllerNote.onTap(noteVOS);

    }

    public interface ControllerNote {
        void onTap(NoteVOS noteVOS);
    }
}
