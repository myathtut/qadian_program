package com.example.myathtut.qadianprogram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.Qadian;
import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.data.vos.NoteVOS;
import com.example.myathtut.qadianprogram.view.NoViewHolder;


import java.util.List;

/**
 * Created by myathtut on 12/12/16.
 */
public class NoteAdapter extends RecyclerView.Adapter<NoViewHolder> {

    private LayoutInflater inflate;
    private List<NoteVOS> noteVOSes;
    private NoViewHolder.ControllerNote controllerNote;

    public NoteAdapter(List<NoteVOS> noteVOSes, NoViewHolder.ControllerNote controllerNote) {
        inflate = LayoutInflater.from(Qadian.getContext());
        this.noteVOSes = noteVOSes;
        this.controllerNote = controllerNote;


    }

    @Override
    public NoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflate.inflate(R.layout.view_item_prayer, parent, false);
        final NoViewHolder noteViewHolders = new NoViewHolder(view,controllerNote);
        return noteViewHolders;
    }

    @Override
    public void onBindViewHolder(NoViewHolder holder, int position) {
        holder.setData(noteVOSes.get(position));

    }

    @Override
    public int getItemCount() {
        return noteVOSes.size();
    }
}
