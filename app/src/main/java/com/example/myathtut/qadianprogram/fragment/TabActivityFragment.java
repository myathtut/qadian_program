package com.example.myathtut.qadianprogram.fragment;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myathtut.qadianprogram.R;
import com.example.myathtut.qadianprogram.adapters.TabAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class TabActivityFragment extends Fragment {
    @BindView(R.id.tl_attractions)
    TabLayout tabLayout;
    @BindView(R.id.tab_pager)
    ViewPager viewPager;

    public TabAdapter tabAdapter;

    public TabActivityFragment() {
    }

    public static TabActivityFragment newInstance() {
        TabActivityFragment tabActivityFragment = new TabActivityFragment();
        return tabActivityFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabAdapter = new TabAdapter(getActivity().getSupportFragmentManager());
        tabAdapter.addTab(MainActivityFragment.newInstance(), getString(R.string.One));
        tabAdapter.addTab(Day2Fragment.newInstance(), getString(R.string.two));
        tabAdapter.addTab(Days3Fragment.newInstance(), getString(R.string.three));
        tabAdapter.addTab(NoteFragment.newInstance(), getString(R.string.note));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab, container, false);
        ButterKnife.bind(this, view);
        viewPager.setAdapter(tabAdapter);
        viewPager.setOffscreenPageLimit(tabAdapter.getCount());
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }
}
