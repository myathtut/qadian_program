package com.example.myathtut.qadianprogram.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myathtut on 11/30/16.
 */
public class TabAdapter extends FragmentPagerAdapter {
    private List<Fragment> mfragments=new ArrayList<>();
    private List<String> mfragmenttitle=new ArrayList<>();

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mfragments.get(position);
    }

    @Override
    public int getCount() {
        return mfragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mfragmenttitle.get(position);
    }
    public void addTab(Fragment fragment,String title)
    {
        mfragments.add(fragment);
        mfragmenttitle.add(title);

    }
}
