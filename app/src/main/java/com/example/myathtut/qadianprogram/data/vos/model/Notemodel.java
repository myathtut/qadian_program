package com.example.myathtut.qadianprogram.data.vos.model;

import com.example.myathtut.qadianprogram.data.vos.Day2VO;
import com.example.myathtut.qadianprogram.data.vos.NoteVOS;
import com.example.myathtut.qadianprogram.util.CommonInstances;
import com.example.myathtut.qadianprogram.util.Jsonutil;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by myathtut on 12/12/16.
 */
public class Notemodel {

    private static final String DUMMY_EVENT_LIST = "prayer.json";
    private static Notemodel objInstance;
    private List<NoteVOS> noteVOSes;

    private Notemodel() {
        noteVOSes = initializeEventList();

    }

    public static Notemodel getInstance() {
        if (objInstance == null) {
            objInstance = new Notemodel();
        }

        return objInstance;
    }

    private List<NoteVOS> initializeEventList() {
        List<NoteVOS> eventList = new ArrayList<>();

        try {
            String dummyEventList = Jsonutil.getInstance().loadDummyData(DUMMY_EVENT_LIST);
            Type listType = new TypeToken<List<NoteVOS>>() {
            }.getType();
            eventList = CommonInstances.getGsonInstance().fromJson(dummyEventList, listType);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public List<NoteVOS> getday1list() {
        return noteVOSes;
    }

    public NoteVOS getEventByTitle(String title) {
        for (NoteVOS dec : noteVOSes) {
            if (dec.getEventTitle().equals(title)) {
                return dec;
            }
        }
        return null;
    }
}


