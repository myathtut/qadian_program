package com.example.myathtut.qadianprogram;

import android.app.Application;
import android.content.Context;

/**
 * Created by myathtut on 11/28/16.
 */
public class Qadian extends Application {

    public static final String TAG = "MyanmarAttractionsApp";

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
